SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `oauth2_client_4s`
-- ----------------------------
DROP TABLE IF EXISTS `system_oauth2_client_4s`;
CREATE TABLE `system_oauth2_client_4s` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_name` varchar(100) DEFAULT NULL,
  `client_id` varchar(100) DEFAULT NULL,
  `client_secret` varchar(100) DEFAULT NULL,
  `status` varchar(2) DEFAULT '1' COMMENT '1-启用 0-未启用',
  `lastUpdAcct`  varchar(20) DEFAULT NULL,
  `lastUpdTime`  varchar(20) DEFAULT NULL ,
  `note`  varchar(200) DEFAULT NULL,	  
  PRIMARY KEY (`id`),
  KEY `idx_oauth2_client_client_id_4s` (`client_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oauth2_client
-- ----------------------------

-- ----------------------------
-- Table structure for `oauth2_token_4s`
-- ----------------------------
DROP TABLE IF EXISTS `system_oauth2_token_4s`;
CREATE TABLE `system_oauth2_token_4s` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `expires` bigint(13) DEFAULT NULL,
  `client_id` varchar(55) DEFAULT NULL,
  `uid` varchar(55) DEFAULT NULL,
  `status` varchar(2) DEFAULT '1' COMMENT '1-启用 0-未启用',
  `lastUpdAcct`  varchar(20) DEFAULT NULL,
  `lastUpdTime`  varchar(20) DEFAULT NULL ,
  `note`  varchar(200) DEFAULT NULL,  
  PRIMARY KEY (`id`),
  KEY `token` (`token`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oauth2_token
-- ----------------------------

-- ----------------------------
-- Table structure for `system_bug`
-- ----------------------------
DROP TABLE IF EXISTS `system_bug_4s`;
CREATE TABLE `system_bug_4s` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(111) DEFAULT NULL,
  `des` text,
  `type` varchar(2) DEFAULT NULL COMMENT '类别',
  `createDate` datetime DEFAULT NULL,
  `status` bigint(20) DEFAULT '1' COMMENT '1.待解决 2. 已处理 3.忽略',
  `lastUpdAcct`  varchar(20) DEFAULT NULL,
  `lastUpdTime`  varchar(20) DEFAULT NULL ,
  `note`  varchar(200) DEFAULT NULL,  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_bug
-- ----------------------------

-- ----------------------------
-- Table structure for `system_log_4s`
-- ----------------------------
DROP TABLE IF EXISTS `system_log_4s`;
CREATE TABLE `system_log_4s` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL,
  `browser` varchar(255) DEFAULT NULL,
  `operation` varchar(2) DEFAULT '0' COMMENT '1.访问 2 登录 3.添加 4. 编辑 5. 删除',
  `from` varchar(255) DEFAULT NULL COMMENT '来源 url',
  `ip` varchar(22) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `status` varchar(2) DEFAULT '1' COMMENT '1-启用 0-未启用',
  `lastUpdAcct`  varchar(20) DEFAULT NULL,
  `lastUpdTime`  varchar(20) DEFAULT NULL ,
  `note`  varchar(200) DEFAULT NULL,  
  PRIMARY KEY (`id`),
  KEY `FK_SYSTEM_EVENT_4s` (`uid`) USING BTREE,
  CONSTRAINT `system_log_ibfk_1_4s` FOREIGN KEY (`uid`) REFERENCES `system_user_4s` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_log
-- ----------------------------

-- ----------------------------
-- Table structure for `system_res`
-- ----------------------------
DROP TABLE IF EXISTS `system_res_4s`;
CREATE TABLE `system_res_4s` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) DEFAULT NULL,
  `name` varchar(111) DEFAULT NULL,
  `des` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `level` int DEFAULT NULL COMMENT '层级',
  `iconCls` varchar(255) DEFAULT 'am-icon-file',
  `seq` bigint(20) DEFAULT '1',
  `type` varchar(2) DEFAULT '2' COMMENT '1 菜单 2 功能',
  `status` varchar(2) DEFAULT '1' COMMENT '1-启用 0-未启用',
  `lastUpdAcct`  varchar(20) DEFAULT NULL,
  `lastUpdTime`  varchar(20) DEFAULT NULL ,
  `note`  varchar(200) DEFAULT NULL,  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_res
-- ----------------------------
-- ----------------------------
-- Table structure for `system_role_4s`
-- ----------------------------
DROP TABLE IF EXISTS `system_role_4s`;
CREATE TABLE `system_role_4s` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(55) DEFAULT NULL,
  `des` varchar(55) DEFAULT NULL,
  `seq` bigint(20) DEFAULT '1',
  `iconCls` varchar(55) DEFAULT 'status_online',
  `pid` bigint(20) DEFAULT '0',
  `createdate` datetime DEFAULT NULL,
  `status` varchar(2) DEFAULT '1' COMMENT '1-启用 0-未启用',
  `lastUpdAcct`  varchar(20) DEFAULT NULL,
  `lastUpdTime`  varchar(20) DEFAULT NULL ,
  `note`  varchar(200) DEFAULT NULL,    
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_role
-- ----------------------------

-- ----------------------------
-- Table structure for `system_role_res_4s`
-- ----------------------------
DROP TABLE IF EXISTS `system_role_res_4s`;
CREATE TABLE `system_role_res_4s` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `res_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_SYSTEM_ROLE_RES_RES_ID_4s` (`res_id`) USING BTREE,
  KEY `FK_SYSTEM_ROLE_RES_ROLE_ID_4s` (`role_id`) USING BTREE,
  CONSTRAINT `system_role_res_ibfk_1_4s` FOREIGN KEY (`res_id`) REFERENCES `system_res_4s` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `system_role_res_ibfk_2_4s` FOREIGN KEY (`role_id`) REFERENCES `system_role_4s` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_role_res
-- ----------------------------

-- ----------------------------
-- Table structure for `system_user_4s`
-- ----------------------------
DROP TABLE IF EXISTS `system_user_4s`;
CREATE TABLE `system_user_4s` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(55) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  `status` varchar(2) DEFAULT '1' COMMENT '#1 正常 2.封号 ',
  `icon` varchar(255) DEFAULT 'images/guest.jpg',
  `email` varchar(255) DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `salt2` varchar(55) DEFAULT NULL,
  `lastUpdAcct`  varchar(20) DEFAULT NULL,
  `lastUpdTime`  varchar(20) DEFAULT NULL ,
  `note`  varchar(200) DEFAULT NULL,	
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_user
-- ----------------------------

-- ----------------------------
-- Table structure for `system_user_role_4s`
-- ----------------------------
DROP TABLE IF EXISTS `system_user_role_4s`;
CREATE TABLE `system_user_role_4s` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_SYSTME_USER_ROLE_USER_ID_4s` (`user_id`) USING BTREE,
  KEY `FK_SYSTME_USER_ROLE_ROLE_ID_4s` (`role_id`) USING BTREE,
  CONSTRAINT `system_user_role_ibfk_1_4s` FOREIGN KEY (`role_id`) REFERENCES `system_role_4s` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `system_user_role_ibfk_2_4s` FOREIGN KEY (`user_id`) REFERENCES `system_user_4s` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_user_role
-- ----------------------------