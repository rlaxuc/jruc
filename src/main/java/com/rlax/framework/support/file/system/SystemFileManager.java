package com.rlax.framework.support.file.system;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import com.rlax.framework.support.file.FileInfo;
import com.rlax.framework.support.file.FileManager;
import com.rlax.framework.support.file.FileResult;

/**
 * 系统文件存储管理器
 * @author Rlax
 *
 */
public class SystemFileManager implements FileManager {

	
	@Override
	public FileResult save(File file, String savePath) {
		return null;
	}

	@Override
	public FileResult save(byte[] data, String savePath) {
		return null;
	}

	@Override
	public FileResult save(InputStream is, String savePath) {
		return null;
	}

	@Override
	public List<FileInfo> list(String dirPath) {
		return null;
	}

	@Override
	public List<FileInfo> list() {
		return null;
	}

	@Override
	public String getDownloadEndpoint() {
		return null;
	}

	@Override
	public String getDownPathByKey(String key) {
		return null;
	}

}
