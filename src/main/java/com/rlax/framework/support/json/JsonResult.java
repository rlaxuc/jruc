package com.rlax.framework.support.json;

import java.io.Serializable;

/**
 * 返回JSON
 * 
 * @author Rlax
 * 
 */
public class JsonResult implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String SUCCESS = "0000";
	public static final String ERROR = "1111";

	public static final String SUCCESS_MSG = "操作成功";
	public static final String ERROR_MSG = "操作失败";

	private String code;
	private String message;
	private Object data = "";

	public JsonResult() {

	}

	public JsonResult(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public void success(String message) {
		this.code = SUCCESS;
		this.message = message;
	}

	public void success() {
		this.code = SUCCESS;
		this.message = SUCCESS_MSG;
	}

	public void error(String message) {
		this.code = ERROR;
		this.message = message;
	}

	public void error() {
		this.code = ERROR;
		this.message = ERROR_MSG;
	}
	
	public boolean isSuccess() {
		if (code == null) {
			return false;
		} else {
			return code.equals(SUCCESS);
		}
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
