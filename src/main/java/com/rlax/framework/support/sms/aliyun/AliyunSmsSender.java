package com.rlax.framework.support.sms.aliyun;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.aliyuncs.sms.model.v20160927.SingleSendSmsRequest;
import com.jfinal.kit.StrKit;
import com.rlax.framework.support.sms.SmsSender;

/**
 * 阿里云短信发送 - 旧版本SDK，在没有外网访问下将无法发送短信，请使用消息服务SMS进行发送
 * 
 * @author Rlax
 * 
 */
public class AliyunSmsSender implements SmsSender {

	private static final Logger logger = LoggerFactory.getLogger(AliyunSmsSender.class);
	
    private String accessId;
    private String accessKey;
    
    private IAcsClient client;
	
    public AliyunSmsSender(String accessId, String accessKey) {
    	this.accessId = accessId;
    	this.accessKey = accessKey;
    	
    	try {
			IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessId, accessKey);
			DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", "Sms", "sms.aliyuncs.com");
			this.client = new DefaultAcsClient(profile);
    	} catch (ServerException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		} catch (ClientException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
    }
    
	@Override
	public void send(String phoneNo, String message) {
		
	}

	@Override
	public void send(List<String> phoneNo, String message) {
		
	}

	@Override
	public void send(String phoneNo, String signName, String templateCode,
			Map<String, String> params) {
		String paramString = JSON.toJSONString(params);
		if (StrKit.isBlank(phoneNo)) {
			logger.error("发送短信号码为空：签名{}, 模版编码{}, 参数串{}", new String[]{signName, templateCode, paramString});
			return;
		}
		
		SingleSendSmsRequest request = new SingleSendSmsRequest();
		request.setSignName(signName);// 控制台创建的签名名称
		request.setTemplateCode(templateCode);// 控制台创建的模板CODE
		request.setParamString(paramString);// 短信模板中的变量；数字需要转换为字符串；个人用户每个变量长度必须小于15个字符。"
		// request.setParamString("{}");
		request.setRecNum(phoneNo);// 接收号码
		
		logger.info("发送短信：接收号码{}, 签名{}, 模版编码{}, 参数串{}", new String[]{phoneNo, signName, templateCode, paramString});
		try {
			client.getAcsResponse(request);
		} catch (ServerException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		} catch (ClientException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
	
	@Override
	public void send(List<String> phoneNoList, String signName,
			String templateCode, Map<String, String> params) {
		String paramString = JSON.toJSONString(params);
		if (phoneNoList == null || phoneNoList.size() == 0) {
			logger.error("发送短信号码为空：签名{}, 模版编码{}, 参数串{}", new String[]{signName, templateCode, paramString});
			return;
		}
		
		StringBuffer phoneStrs = new StringBuffer();
		for (String phoneNo : phoneNoList) {
			phoneStrs.append(phoneNo).append(",");
		}
		
		phoneStrs.substring(0, phoneStrs.length() - 1);
		send(phoneStrs.toString(), signName, templateCode, params);
	}

	public String getAccessId() {
		return accessId;
	}

	public void setAccessId(String accessId) {
		this.accessId = accessId;
	}

	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}
}
