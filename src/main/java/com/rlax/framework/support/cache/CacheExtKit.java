package com.rlax.framework.support.cache;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.shiro.cache.CacheException;
import org.apache.shiro.util.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jfinal.plugin.ehcache.CacheKit;
import com.jfinal.plugin.redis.Redis;

/**
 * 封装缓存，统一缓存
 * @author Rlax
 *
 */
public class CacheExtKit {

	private final static Logger logger = LoggerFactory.getLogger(CacheExtKit.class);
	
	/** 默认 ehcache */
	public static String TYPE = "ehcache";
	
	/** redis 缓存 */
	public final static String REDIS_TYPE = "redis";
	/** ehcache 缓存 */
	public final static String EHCACHE_TYPE = "ehcache";
	/** ehcache + redis 二级缓存 */
	public final static String L2_TYPE = "L2";
	
	private static CacheExtKit me = new CacheExtKit();
	
	private CacheExtKit() {}
	
	public static CacheExtKit use(String type) {
		TYPE = type;
		return me;
	}
	
	public static <T> T get(String cacheName, Object key) {
		T value = null;
		if (key == null) {
            return null;
        } else {
        	if (TYPE.equals(REDIS_TYPE)) {
        		value = Redis.use().get(cacheName + "_" + key);
        		logger.debug("cache {} get key {}_{}", new Object[]{TYPE, cacheName, key, value});
        	} else if (TYPE.equals(EHCACHE_TYPE)) {
        		value = CacheKit.get(cacheName, key);
        		logger.debug("cache {} get key {}_{}", new Object[]{TYPE, cacheName, key, value});
        	} else if (TYPE.equals(L2_TYPE)) {
        		value = CacheKit.get(cacheName, key);
        		if (value == null) {
        			value = Redis.use().get(cacheName + "_" + key);
        			if (value != null) {
        				CacheKit.put(cacheName, key, value);
        			}
        			logger.debug("cache {} from redis get key {}_{} value {}", new Object[]{TYPE, cacheName, key, value});
        		} else {
        			logger.debug("cache {} from ehcache get key {}_{} value {}", new Object[]{TYPE, cacheName, key, value});
        		}
        	} else {
        		throw new CacheException("not support cache type " + TYPE);
        	}
        }
		return value;
	}

	public static void put(String cacheName, Object key, Object value) {
	  	logger.debug("cache {} put key {}_{}, value {}", new Object[]{TYPE, cacheName, key, value});
    	if (TYPE.equals(REDIS_TYPE)) {
    		Redis.use().set(cacheName + "_" + key, value);
    	} else if (TYPE.equals(EHCACHE_TYPE)) {
    		CacheKit.put(cacheName, key, value);
    	} else if (TYPE.equals(L2_TYPE)) {
    		CacheKit.put(cacheName, key, value);
    		Redis.use().set(cacheName + "_" + key, value);
    	}
	}

	public static void remove(String cacheName, Object key) {
    	logger.debug("cache {} delete key {}_{}", new Object[]{TYPE, cacheName, key});
        if (key != null) {
        	if (TYPE.equals(REDIS_TYPE)) {
        		Redis.use().del(cacheName + "_" + key);
        	} else if (TYPE.equals(EHCACHE_TYPE)) {
        		CacheKit.remove(cacheName, key);
        	} else if (TYPE.equals(L2_TYPE)) {
        		CacheKit.remove(cacheName, key);
        		Redis.use().del(cacheName + "_" + key);
        	}
        }
	}

	public static void removeAll(String cacheName) {
    	logger.debug("cache {} clear all keys", TYPE);
    	
    	if (TYPE.equals(REDIS_TYPE)) {
            Set<String> keys = Redis.use().keys(cacheName + "_" + "*");
            Redis.use().del(keys.toArray());
    	} else if (TYPE.equals(EHCACHE_TYPE)) {
    		CacheKit.removeAll(cacheName);
    	} else if (TYPE.equals(L2_TYPE)) {
    		CacheKit.removeAll(cacheName);
            Set<String> keys = Redis.use().keys(cacheName + "_" + "*");
            Redis.use().del(keys.toArray());
    	}
	}	
	
	public static int size(String cacheName) {
		int size = 0;
		
    	if (TYPE.equals(REDIS_TYPE)) {
            Set<String> keys = Redis.use().keys(cacheName + "_*");
            size = keys.size();
    	} else if (TYPE.equals(EHCACHE_TYPE)) {
    		size = CacheKit.getKeys(cacheName).size();
    	} else if (TYPE.equals(L2_TYPE)) {
            Set<String> keys = Redis.use().keys(cacheName + "_*");
            size = keys.size();
    	}
    	
    	logger.debug("cache {} size {}", TYPE, size);
    	return size;
	}

	@SuppressWarnings("unchecked")
	public static Set<String> keys(String cacheName) {
		logger.debug("cache {} keys", TYPE);
		
		Set<String> keys = Collections.emptySet();
    	if (TYPE.equals(REDIS_TYPE)) {
            keys = Redis.use().keys(cacheName + "_*");
    	} else if (TYPE.equals(EHCACHE_TYPE)) {
    		keys = Collections.unmodifiableSet(new LinkedHashSet<String>(CacheKit.getKeys(cacheName)));
    	} else if (TYPE.equals(L2_TYPE)) {
    		keys = Redis.use().keys(cacheName + "_*");
    	}
    	return keys;
	}

	@SuppressWarnings("unchecked")
	public static Collection<Object> values(String cacheName) {
		logger.debug("cache {} values", TYPE);
		
		Collection<Object> values = Collections.emptyList();
    	if (TYPE.equals(REDIS_TYPE)) {
        	Set<String> keys = Redis.use().keys(cacheName + "_*");
        	values = new ArrayList<Object>(keys.size());
            for (Object key : keys) {
            	Object value = CacheKit.get(cacheName, key);
                if (value != null) {
                    values.add(value);
                }
            }            	
    	} else if (TYPE.equals(EHCACHE_TYPE)) {
            List<Object> keys = CacheKit.getKeys(cacheName);
            if (!CollectionUtils.isEmpty(keys)) {
            	values = new ArrayList<Object>(keys.size());
                for (Object key : keys) {
                	Object value = CacheKit.get(cacheName, key);
                    if (value != null) {
                        values.add(value);
                    }
                }
            }	    	
        } else if (TYPE.equals(L2_TYPE)) {
        	Set<String> keys = Redis.use().keys(cacheName + "_*");
        	values = new ArrayList<Object>(keys.size());
            for (Object key : keys) {
            	Object value = CacheKit.get(cacheName, key);
                if (value != null) {
                    values.add(value);
                }
            }	    	
        }
    	return values;
	}
}
