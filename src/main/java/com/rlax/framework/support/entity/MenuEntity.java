package com.rlax.framework.support.entity;

import com.rlax.web.model.Res4s;

/**
 * 导航菜单实体
 * @author Rlax
 *
 */
public class MenuEntity {

	private Res4s res;
	private boolean hasChild = false;
	
	public MenuEntity() {
		
	}

	public MenuEntity(Res4s res) {
		this.res = res;
	}
	
	public Res4s getRes() {
		return res;
	}
	public void setRes(Res4s res) {
		this.res = res;
	}
	public boolean isHasChild() {
		return hasChild;
	}
	public void setHasChild(boolean hasChild) {
		this.hasChild = hasChild;
	}
}
