package com.rlax.framework.generator;

import javax.sql.DataSource;

import com.jfinal.plugin.activerecord.generator.MetaBuilder;

/**
 * 自定义 MetaBuilder，处理表前缀跳过生成
 * @author Rlax
 *
 */
public class GeMetaBuilder extends MetaBuilder {

	/**
	 * 需要跳过生成的表前缀
	 */
	private String[] skipPre = null;
	
	public GeMetaBuilder(DataSource dataSource) {
		super(dataSource);
	}

	@Override
	protected boolean isSkipTable(String tableName) {
		for (String skip : skipPre) {
			if (tableName.startsWith(skip)) {
				return true;
			}
		}
		return false;
	}

	public void setSkipPre(String[] skipPre) {
		this.skipPre = skipPre;
	}
}
