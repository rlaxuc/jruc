package com.rlax.framework.plugin.reids;

import java.io.Serializable;

import com.jfinal.captcha.Captcha;

/**
 * redis 验证码
 * @author Rlax
 *
 */
public class RedisCaptcha extends Captcha implements Serializable {

	private static final long serialVersionUID = 8291005227523964710L;

	public RedisCaptcha(String key, String value) {
		super(key, value);
	}
	
	public RedisCaptcha(String key, String value, int expireTime) {
		super(key, value, expireTime);
	}

}
