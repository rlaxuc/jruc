package com.rlax.framework.plugin.quartz;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;

/**
 * Job 基类
 * @author Rlax
 *
 */
public abstract class BaseJob extends AbstractJob {
	private static final Logger log = Logger.getLogger(BaseJob.class);

    private Long start = null;
    private Long end = null;
	
	@Override
	protected void preExecute(JobExecutionContext context) {
		start = System.currentTimeMillis();
		log.info("start job : " + context.getJobDetail().getJobClass().getName());
	}

	@Override
	protected void afterExecute(JobExecutionContext context) {
		end = System.currentTimeMillis();
		log.info("end job : " + context.getJobDetail().getJobClass().getName());
		log.info("use time : " + String.valueOf(end - start) + " ms");
	}
	
}
