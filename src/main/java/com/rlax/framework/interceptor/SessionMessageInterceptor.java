package com.rlax.framework.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.rlax.framework.common.Consts;
import com.rlax.web.base.BaseController;
import com.rlax.web.base.MessageBean;

/**
 * Session Message 拦截器
 * 将重定向请求中保存在 Session 的消息加入 到Param中
 * @author Rlax
 *
 */
public class SessionMessageInterceptor implements Interceptor {

	@Override
	public void intercept(Invocation inv) {
		if (inv.getController() instanceof BaseController) {
			BaseController c = (BaseController) inv.getController();
			MessageBean message = c.getSessionAttr(Consts.SESSION_MESSAGE);
			if (message != null) {
				c.removeSessionAttr(Consts.SESSION_MESSAGE);
				c.setAttr(Consts.SESSION_MESSAGE, message);
			}
		} 
		inv.invoke();
	}

}
