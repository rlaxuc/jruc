package com.rlax.web.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseRes4s<M extends BaseRes4s<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Long id) {
		set("id", id);
	}

	public java.lang.Long getId() {
		return get("id");
	}

	public void setPid(java.lang.Long pid) {
		set("pid", pid);
	}

	public java.lang.Long getPid() {
		return get("pid");
	}

	public void setName(java.lang.String name) {
		set("name", name);
	}

	public java.lang.String getName() {
		return get("name");
	}

	public void setDes(java.lang.String des) {
		set("des", des);
	}

	public java.lang.String getDes() {
		return get("des");
	}

	public void setUrl(java.lang.String url) {
		set("url", url);
	}

	public java.lang.String getUrl() {
		return get("url");
	}

	public void setLevel(java.lang.Integer level) {
		set("level", level);
	}

	public java.lang.Integer getLevel() {
		return get("level");
	}

	public void setIconCls(java.lang.String iconCls) {
		set("iconCls", iconCls);
	}

	public java.lang.String getIconCls() {
		return get("iconCls");
	}

	public void setSeq(java.lang.Long seq) {
		set("seq", seq);
	}

	public java.lang.Long getSeq() {
		return get("seq");
	}

	public void setType(java.lang.String type) {
		set("type", type);
	}

	public java.lang.String getType() {
		return get("type");
	}

	public void setStatus(java.lang.String status) {
		set("status", status);
	}

	public java.lang.String getStatus() {
		return get("status");
	}

	public void setLastUpdAcct(java.lang.String lastUpdAcct) {
		set("lastUpdAcct", lastUpdAcct);
	}

	public java.lang.String getLastUpdAcct() {
		return get("lastUpdAcct");
	}

	public void setLastUpdTime(java.lang.String lastUpdTime) {
		set("lastUpdTime", lastUpdTime);
	}

	public java.lang.String getLastUpdTime() {
		return get("lastUpdTime");
	}

	public void setNote(java.lang.String note) {
		set("note", note);
	}

	public java.lang.String getNote() {
		return get("note");
	}

}
