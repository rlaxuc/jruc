package com.rlax.web.model;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;

/**
 * Generated by JFinal, do not modify this file.
 * <pre>
 * Example:
 * public void configPlugin(Plugins me) {
 *     ActiveRecordPlugin arp = new ActiveRecordPlugin(...);
 *     _MappingKit.mapping(arp);
 *     me.add(arp);
 * }
 * </pre>
 */
public class _MappingKit {

	public static void mapping(ActiveRecordPlugin arp) {
		arp.addMapping("system_bug_4s", "id", Bug4s.class);
		arp.addMapping("system_log_4s", "id", Log4s.class);
		arp.addMapping("system_oauth2_client_4s", "id", Oauth2Client4s.class);
		arp.addMapping("system_oauth2_token_4s", "id", Oauth2Token4s.class);
		arp.addMapping("system_res_4s", "id", Res4s.class);
		arp.addMapping("system_role_4s", "id", Role4s.class);
		arp.addMapping("system_role_res_4s", "id", RoleRes4s.class);
		arp.addMapping("system_user_4s", "id", User4s.class);
		arp.addMapping("system_user_role_4s", "id", UserRole4s.class);
		arp.addMapping("website_data", "id", Data.class);
		arp.addMapping("website_page", "id", Page.class);
		arp.addMapping("website_res", "id", Res.class);
		arp.addMapping("website_role", "id", Role.class);
		arp.addMapping("website_user", "id", User.class);
		arp.addMapping("website_weixin_user", "id", WeixinUser.class);
	}
}

