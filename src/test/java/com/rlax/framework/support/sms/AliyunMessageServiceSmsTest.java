package com.rlax.framework.support.sms;

import java.util.HashMap;
import java.util.Map;

import com.rlax.framework.support.sms.aliyun.AliyunMessageServiceSmsSender;

public class AliyunMessageServiceSmsTest {

	public static void main(String[] args) {
		SmsSender sender = new AliyunMessageServiceSmsSender("xxx", "xxx", "https://1962790071204690.mns.cn-beijing.aliyuncs.com/", "sms.topic-cn-beijing");
		Map<String, String> params = new HashMap<String, String>();
		params.put("customer", "xxx");
		sender.send("xxx", "xxx", "xxx", params);
	}
}
